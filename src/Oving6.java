import java.io.*;
import java.net.URL;
import java.util.StringTokenizer;

public class Oving6 {

    int N, K;
    Node []node;



    public static class Forgj{
        int dist;
        Node forgj;
        static int uendelig = 1000000000;

        public Forgj() {
            dist = uendelig;
        }
    }


    static class Kant {
        Kant neste;
        Node til;
        public Kant(Node n, Kant nst) {
            til = n;
            neste = nst;
        }
    }

    public static class  Node {
        Kant kant1;
        Object d;
        int nodeID;

        @Override
        public String toString() {
            return " " + d;
        }

    }

//////////////OOOOOOOOOOOKKKKKKKKKKKKKKKK////////////////////////////
    public static class Kø {

        KøNode hode;
        public int antall;

        public boolean tom(){
        return hode==null;
        }

        public void leggTilBakerst(Node node){
            KøNode køNode = new KøNode(node);
            if(hode==null){
                hode = køNode;
                return;
            }
            KøNode denne = hode;
            while(denne.neste()!=null){
            denne = denne.neste();
            }
            denne.neste = køNode;
            antall++;
        }

        public Node hentFremste(){
            return hode.node();
        }

        public Node fjernFremste(){
            KøNode retur = hode;
            hode = hode.neste();
            retur.neste = null;
            return retur.node();
        }

        private static class KøNode{
            KøNode neste;
            Node node;

            public KøNode(Node node){
                this.neste = null;
                this.node = node;
            }

            public KøNode neste(){return neste;}

            public Node node(){return node;}
        }

    }

    public void initforgj(Node s) {
        for (int i = N; i-->0;) {
            node[i].d= new Forgj();
        }
        ((Forgj)s.d).dist = 0;
    }

    public void bfs(Node s){
        initforgj(s);
        Kø kø = new Kø();
        kø.leggTilBakerst(s);
        while (!kø.tom()){
            Node n = kø.hentFremste();
            for (Kant k = n.kant1; k != null; k = k.neste){
                Forgj f = (Forgj)k.til.d;
                if (f.dist == Forgj.uendelig){
                    f.dist = ((Forgj)n.d).dist + 1;
                    f.forgj = n;
                    kø.leggTilBakerst(k.til);
                }
            }
            kø.fjernFremste();
        }
    }

    public static class Topo {
        boolean funnet;
        Node neste;
    }

    public Node dftopo(Node n, Node l){
        Topo nd = (Topo) n.d;
        if(nd.funnet) return l;
        nd.funnet = true;
        for(Kant k = n.kant1;k!=null;k=k.neste){
            l = dftopo(k.til,l);
        }
        nd.neste = l;
        System.out.println(n.nodeID);
        return n;
    }

    public Node topologisort(){
        Node l = null;
        for(int i=N;i-->0;){
            node[i].d = new Topo();
        }
        for (int i=N;i-->0;){
            l = dftopo(node[i],l);
        }
        return l;
    }

    //innlesing fra fil
    public void ny_ugraf(URL url) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        StringTokenizer st = new StringTokenizer(br.readLine());
        N = Integer.parseInt(st.nextToken());
        node = new Node[N];
        for (int i=0; i<N; ++i) {
            node[i] = new Node();
            node[i].nodeID=i;
        }
        K = Integer.parseInt(st.nextToken());
        for (int i=0; i<K; ++i) {
            st = new StringTokenizer(br.readLine());
            int fra = Integer.parseInt(st.nextToken());
            int til = Integer.parseInt(st.nextToken());
            node[fra].kant1 = new Kant(node[til], node[fra].kant1);

        }
    }

    public void lesFraUrl(){
        System.out.println("Node:   Forgj:   Dist:");
        for (int i = 0; i < N; i++){
            int forgj;
            if(((Forgj)node[i].d).forgj == null){
                forgj = -1;
            }else{
                forgj = ((Forgj)node[i].d).forgj.nodeID;
            }
            System.out.println("      "+node[i].nodeID + "      " + forgj + "       " + ((Forgj)node[i].d).dist);
        }
    }


    public  String toString(){
        String string = "";
        for(int i =0; i<node.length;i++){
            if(node[i].kant1 != null){
                string += "\nNode:"+i +"Forrige: "+ node[i].nodeID+" "+
                "Distanse: "+ node[i].d;
            }
            else{
                string += "\nNode: " + i + " Forgjenger: "+
                        "Distanse: " + node[i].d;
            }
        }
        return string;
    }


    public static void main(String[] args) throws IOException {
        Oving6 graf = new Oving6();
        int startNummer = 5;

        URL url = new URL("http://www.iie.ntnu.no/fag/_alg/uv-graf/L7g1");
        graf.ny_ugraf(url);
        graf.bfs(graf.node[startNummer]);
        graf.lesFraUrl();

        System.out.println("\n Topologisort: ");
        graf.topologisort();
    }


}
