import java.io.*;
import java.util.*;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.stream.Collectors;

public class BFS {
    private static ArrayList<Integer> array;



    /**
     * An implementation of BFS with an adjacency list.
     *
     * <p>Time Complexity: O(V + E)
     *
     *
     */






        public static class Edge {
            int from, to, cost;

            public Edge(int from, int to, int cost) {
                this.from = from;
                this.to = to;
                this.cost = cost;
            }
        }

        private int n;
        private Integer[] prev;
        private List<List<Edge>> graph;


        public BFS(List<List<Edge>> graph) {
            if (graph == null) throw new IllegalArgumentException("Graph can not be null");
            n = graph.size();
            this.graph = graph;
        }

        /**
         * Reconstructs the path (of nodes) from 'start' to 'end' inclusive. If the edges are unweighted
         * then this method returns the shortest path from 'start' to 'end'
         *
         * @return An array of nodes indexes of the shortest path from 'start' to 'end'. If 'start' and
         *     'end' are not connected then an empty array is returned.
         */
        public List<Integer> reconstructPath(int start, int end) {
            bfs(start);
            List<Integer> path = new ArrayList<>();
            for (Integer at = end; at != null; at = prev[at]) path.add(at);
            Collections.reverse(path);
            if (path.get(0) == start) return path;
            path.clear();
            return path;
        }

        // Perform a breadth first search on a graph a starting node 'start'.
        private void bfs(int start) {
            prev = new Integer[n];
            boolean[] visited = new boolean[n];
            Deque<Integer> queue = new ArrayDeque<>(n);

            // Start by visiting the 'start' node and add it to the queue.
            queue.offer(start);
            visited[start] = true;

            // Continue until the BFS is done.
            while (!queue.isEmpty()) {
                int node = queue.poll();
                List<Edge> edges = graph.get(node);

                // Loop through all edges attached to this node. Mark nodes as visited once they're
                // in the queue. This will prevent having duplicate nodes in the queue and speedup the BFS.
                for (Edge edge : edges) {
                    if (!visited[edge.to]) {
                        visited[edge.to] = true;
                        prev[edge.to] = node;
                        queue.offer(edge.to);
                    }
                }
            }
        }

        // Initialize an empty adjacency list that can hold up to n nodes.
        public static List<List<Edge>> createEmptyGraph(int n) {
            List<List<Edge>> graph = new ArrayList<>(n);
            for (int i = 0; i < n; i++) graph.add(new ArrayList<>());
            return graph;
        }

        // Add a directed edge from node 'u' to node 'v' with cost 'cost'.
        public static void addDirectedEdge(List<List<Edge>> graph, int u, int v, int cost) {
            graph.get(u).add(new Edge(u, v, cost));
        }

        // Add an undirected edge between nodes 'u' and 'v'.
        public static void addUndirectedEdge(List<List<Edge>> graph, int u, int v, int cost) {
            addDirectedEdge(graph, u, v, cost);
            addDirectedEdge(graph, v, u, cost);
        }

        // Add an undirected unweighted edge between nodes 'u' and 'v'. The edge added
        // will have a weight of 1 since its intended to be unweighted.
        public static void addUnweightedUndirectedEdge(List<List<Edge>> graph, int u, int v) {
            addUndirectedEdge(graph, u, v, 1);
        }


        public  static  void addFromFile(String filename) throws IOException {


        }

        /* BFS example. */

        public static void main(String[] args) throws IOException {
            // BFS example #1 from slides.



            Scanner scanner = new Scanner(new File("C:\\Users\\Frode\\OneDrive - NTNU\\Algoritmer og datastrukturer\\Oving6\\L7g1.txt"));
            // antall noder!
            final int n = 21;

            List<List<Edge>> graph = createEmptyGraph(n);
            int i = 0;
            while(scanner.hasNextInt()){
                            ///                 under første scanne tall, så andre
                addUnweightedUndirectedEdge(graph,scanner.nextInt(),scanner.nextInt());
            }


            BFS solver;
            solver = new BFS(graph);
            //fra - til
            int start = 0, end = 6;
            List<Integer> path = solver.reconstructPath(start, end);
            System.out.printf("The shortest path from %d to %d is: [%s]\n", start, end, formatPath(path));
            // Prints:
            // The shortest path from 10 to 5 is: [10 -> 9 -> 0 -> 7 -> 6 -> 5]

        }

        private static String formatPath(List<Integer> path) {
            return path.stream().map(Object::toString).collect(Collectors.joining(" -> "));
        }
    }

